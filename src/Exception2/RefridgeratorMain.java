package Exception2;

public class RefridgeratorMain {

	public static void main(String[] args) {
		Refrigerator ref = new Refrigerator(5);
		try {
			// put elements to arraylist
			ref.put("apple");
			ref.put("grape");
			ref.put("tomato");
			ref.put("cucumber");
			ref.put("orange");
			
			// show list before take out 
			System.out.println("--- In Refridgerator ---");
			System.out.println(ref.toString());
			
			// take elements take out from arraylist
			System.out.println("--- Take out ---");
			System.out.println(ref.takeOut("apple"));
			System.out.println(ref.takeOut("pineapple"));
			System.out.println();
			
			// show list after take out from arraylist
			System.out.println("--- After take out ---");
			System.out.println(ref.toString());
			
			// add element to arraylist again
			System.out.println();
			System.out.println("--- put in Refridgerator ---");
			ref.put("watermelon");
			ref.put("melon");
			
		}
		catch (FullException e){
			System.err.println("Error: " + e.getMessage());
		}	
	}
}
