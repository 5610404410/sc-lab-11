package Exception1;

public class FormatException extends NullPointerException{
	public FormatException(){
		super();
	}
	
	public FormatException(String message){
		super(message);
	}
}
