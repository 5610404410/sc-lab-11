package Exception1;


public class Main {

	public static void main(String[] args) {
		try{
			MyClass c = new MyClass();
			System.out.print("A");
			c.methX();
			System.out.print("B");
			c.methY();
			System.out.print("C");
			return;
		} 
		catch (DataException e){
			System.out.print("D");
		} 
		catch (FormatException e){
			System.out.print("E");
		} 
		finally {
			System.out.print("F");
		}
		
		System.out.print("G");
	}

}

/* --- ตอบคำถาม ข้อ 1-4 ---
 1. ที่ methX ไม่สามารถ throw DataException และ ที่ methY 
 	ไม่สามารถ throw new FormatException ได้ เนื่องจากไม่มี Exception นั้น 
 	จึงต้องทำการสร้าง Exception นั้นก่อน
 2. A B C F 
 3. A D F G
 4. A B E F G 
 */
  
