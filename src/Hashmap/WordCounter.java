package Hashmap;

import java.util.ArrayList;
import java.util.HashMap;

public class WordCounter {
	private ArrayList<String> listWord;
	private String message;
	private HashMap<String,Integer> wordCount;
	
	public WordCounter(String message){
		this.message = message;
		wordCount = new HashMap<String,Integer>();
		listWord = new ArrayList<String>();
	}
	
	public void count(){
		String[] word = message.split(" ");
		for (int i=0; i<word.length; i++){
			listWord.add(word[i]);
		}
		int count = 0;
		for (String x : listWord){
			count = 0;
			for (String y : listWord)
				if (y.equals(x)){
					count++;
					
			}
//			System.out.println(x);
//			System.out.println(count);
			wordCount.put(x, count);
			
		}
	}
	
	public String hasWord(String word){
		if (wordCount.containsKey(word) == false){return "key : " + word + " value : 0";}
		else {return "key : " + word + " value : " + wordCount.get(word);}
	}
}
